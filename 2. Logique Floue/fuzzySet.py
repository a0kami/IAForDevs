#!/usr/bin/python3

from shapely.geometry import Point

class FuzzySet:
  """ Defining a FuzzySet class to experiment with IA models
  Initializes a list, and 2 values xMin and xMax
  Redefines most binary operators """

  log = True

  # CTOR
  def __init__(self, xMin, xMax):
    self.pList = []
    self.xMin = xMin
    self.xMax = xMax
  
  # Add a point
  def pAdd(self, pt):
    self.pList.append(pt)
    self.pList.sort(key=lambda pt: pt.x)
  
  #############
  # OPERATORS #
  #############
  
  # String reprensentation
  def __str__(self):
    stringToReturn = ["[", str(self.xMin), ";", str(self.xMax), "]:"]
    for pt in self.pList:
      stringToReturn.append("\n\t" + str(pt))
    return("".join(stringToReturn))
  
  # == Operator
  def __eq__(self, other):
    return(str(self) == str(other))
    
  # != Operator
  def __ne__(self, other):
    return(not (self == other))
    
  # [FuzzySet] * [int|float] operator
  def __mul__(self, other):
    if(type(self) is FuzzySet and (type(other) is float or type(other) is int) ):
      result = FuzzySet(self.xMin, self.xMax)
      for pt in self.pList:
        result.pAdd(Point(pt.x, pt.y * other))
      return result
    else:
      raise Exception("FuzzySet * Not Supported")
  
  # [int|float] * [FuzzySet] operator  
  def __rmul__(self, other):
    if(type(self) is FuzzySet and (type(other) is float or type(other) is int) ):
      return(self * other)
    else:
      raise Exception("FuzzySet * Not Supported")
  
  # ~ Operator to get the NOT fuzzy set operator ((x, 1 - y) for each point)
  def __invert__(self):
    result = FuzzySet(self.xMin, self.xMax)
    for pt in self.pList:
      result.pAdd(Point(pt.x, 1 - pt.y))
    return result

  # Zadeh's AND and OR operators (recpectively Intersection and Union)
  def __and__(self, fs1, fs2):
    return merge(fs1, fs2, key=min())
  def __or__(self, fs1, fs2):
    return merge(fs1, fs2, key=max())
    

  ###########
  # METHODS #
  ###########
  
  # Gets the y for the x input
  def degreeAtValue(self, xValue):
    # Case 1: outside of interval
    if(xValue < self.xMin or xValue > self.xMax):
      return(0)
    
    # Generators for respectively LastOrDfault and FirstOrDefault C# Enumerable interface methods
    # Before gets the last point to respect 'point.x <= xValue' condition
    beforeConditionList = list((pt.x <= xValue) for pt in reversed(self.pList))
    beforeIndex = (len(self.pList) - 1) - beforeConditionList.index(True)
    before = self.pList[beforeIndex]
    # After gets the first point to respect 'point.x >= xValue' condition
    afterConditionList = list((pt.x >= xValue) for pt in self.pList)
    afterIndex = afterConditionList.index(True)
    after = self.pList[afterIndex]
    # 'ValueError: True is not in list' is raised if xValue outside of interval {pList[0].x, pList[-1].x}
    
    if(self.log):
      print("".join(["xValue: ", str(xValue), "\n\tBefore: pList[", str(beforeIndex), "] = ", str(before), " (", str(list(reversed(beforeConditionList))), ")\n\tAfter: pList[", str(afterIndex), "] = ", str(after), " (", str(afterConditionList), ")"]))
    
    # Case 2: both before and after points are the same
    if(before == after):
      return(before.y)

    # Case 3: we need to interpolate
    else:
      result = (((before.y - after.y) * (after.x - xValue)) / (after.x - before.x)) + after.y
      return result
  
  # Merging 2 FuzzySets
  def merge(fs1, fs2, key=None):
    # Creation of a new fuzzyset
    result = FuzzySet(min(fs1.xMin, fs2.xMin), max(fs1.xMax, fs2.xMax))
    
    # Need 2 iterator over the 2 fuzzysets
    iter1, iter2 = iter(fs1), iter(fs2)
    
    return(result)

# Main ?
p1 = Point(0.1, 0.1)
p2 = Point(0.2, 0.2)
p3 = Point(0.3, 0.3)
ptList = [p1, p2, p3]



test = FuzzySet(0, 10)
test.pAdd(p3)
test.pAdd(p2)
test.pAdd(p1)

print(test, "\n")
print(test.degreeAtValue(0.15))
print(test.degreeAtValue(0.25))
print(test.degreeAtValue(0.2))
#print(test.degreeAtValue(0.09))

