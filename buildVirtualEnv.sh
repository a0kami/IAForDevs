############
# Activate #
############
function activateVE() {
  # Check if not already activated
  env | grep "($1)" 2>&1 > /dev/null
  if [ $? -ne 0 ]
  then
    source $1/bin/activate
    echo "Activated virtual environment shell."
  else
    echo "Already in the virtual environment shell."
  fi
}

##############
# Deactivate #
##############
function deactivateVE() {
  # Check if activated
  env | grep "($1)" 2>&1 > /dev/null
  if [ $? -eq 0 ]
  then
    deactivate
    echo "Virtual Environment shell deactivated."
  else
    echo "Not in an virtual environment shell."
  fi
}

################
# CHECK PYTHON #
################
function checkPython() {
  # Python
  python3 --version 2>&1 > /dev/null
  # Python3 not found
  if [ ! $? -eq 0 ]
  then
    echo "python3 not found\n"
    exit 0
  fi

  # Pip
  pip3 --version 2>&1 > /dev/null
  # Pip3 not found
  if [ ! $? -eq 0 ]
  then
    echo "pip3 not found\n"
    exit 0
  fi
}

#########
# BUILD #
#########
function buildVirtualEnv()
{
  # Check for virtualEnv
  isVirtualenvInstalled=`pip2 list 2>/dev/null | grep virtualenv | wc -l`
  if [ $((isVirtualenvInstalled)) -eq 0 ]
  then
    echo -e "VirtualEnv Python3 modules not found.\nIn order to not execute this makefile as root, you should run the following command yourself:\nsudo pip3 install virtualenv"
    exit 0
  fi

  # Get virtualenv version
  virtualEnvVersion=`pip2 list 2>/dev/null | grep virtualenv | sed "s/.*(\(.*\))/\1/"`
  pythonVersion=`python3 --version`
  echo "Using virtualenv $virtualEnvVersion for $pythonVersion..."

  echo "Build virtual environment to directory \"$1\"..."
  virtualenv -qp python3 $1 2> /dev/null

  # Fail ?
  if [ $? -ne 0 ]
  then
    echo "Failed building virtual environment in $1."
    exit "-1"
  fi

  # Activate virtualenv prompt
  activateVE $1
}

#########
# CLEAN #
#########
function cleanVirtualEnv()
{
  # Deactivate virtual environment
  deactivateVE $1

  # Check if input directory is the right one
  buildDirectory=$1

  # Clean
  read -p "This will remove $1 and everything in it, are you sure ? [y/N] " confirmClean
  if [[ $confirmClean == "y" ]] || [[ $confirmClean == "yes" ]] || [[ $confirmClean == "Y" ]] || [[ $confirmClean == "Yes" ]] || [[ $confirmClean == "YES" ]]
  then
    echo "Cleaning \"$buildDirectory\"..."
    rm -rf $buildDirectory
    echo "VirtualEnv removed."
  else
    echo "Cleaning aborted."
  fi
}

########
# MAIN #
########

# Help
if [[ $1 == "-h" ]] || [[ $1 == "--help" ]] || [[ $1 == "h" ]] || [[ $1 == "help" ]]
then
  echo -e "Build a python3 virtual environment to use IA examples.\nUsage:\n\t./buildVirtualEnv [build (default)|clean] [<directory> (default is \"ve\")]\n"
  exit 0
fi

# Directory
if [ $# -eq "2" ]
then
  buildDirectory=$2
else
  buildDirectory="ve"
fi

# Build or Clean
if [[ $1 == "clean" ]]
then
  #buildDirectory=checkBuildDirectory $buildDirectory
  cleanVirtualEnv $buildDirectory
else
  checkPython
  buildVirtualEnv $buildDirectory
fi
